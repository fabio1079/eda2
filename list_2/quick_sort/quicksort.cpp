#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

struct TNo{
    struct TNo *prev;
    int value;
    struct TNo *next;
};

struct TList{
	struct TNo *start;
    struct TNo *head;
    struct TNo *first;
    struct TNo *second;
    struct TNo *tail;
};

TList * createTList(TNo * head, TNo * tail){
    TList * list = (TList *) malloc(sizeof(TList));
    list->start = head;
    list->head = head;
    list->first = NULL;
    list->second = NULL;
    list->tail = tail;
    return list;
}

TNo * createTNo(int data, TNo * prev){
    TNo * no = (TNo *) malloc(sizeof(TNo));
    no->value = data;
    if(prev == NULL){
	  no->prev = NULL;
	  no->next = NULL;
    }else if(prev->next == NULL){
	  no->prev = prev;
	  no->next = NULL;
	  prev->next = no;
    }else{
	  no->prev = prev;
	  no->next = prev->next;
	  prev->next->prev = no;
	  prev->next = no;
    }
    return no;
}

int before(TNo * first, TNo * second){
    while(first->next != NULL){
	  if(first->next == second){
		return 1;
	  }
	  first = first->next;
    }
    return 0;
}

int beforeEqual(TNo * first, TNo * second){
    while(first != NULL){
	  if(first == second){
		return 1;
	  }
	  first = first->next;
    }
    return 0;
}

void printInfo(TNo * no){
	printf("%x <- %x -> %x\n",&(no->prev), &(no), &(no->next));
	printf("%x <= %x => %x\n",&(no->prev->value), &(no->value), &(no->next->value));
	printf("%d == %d == %d\n", no->prev->value, no->value, no->next->value);
	printf("________________\n");
}

void listAllContacts(TNo * head){
    printf("\n");
    if(head != NULL){
	  printf("%d\n---\n",head->value);
	  do{
		printf("%d\n---\n",head->next->value);
		head = head->next;
	  }while(head->next != NULL);
    }    
}

TList * swap(TList * list, TNo * first, TNo * second){
	int swapHead = 0;
	int swapTail = 0;
	int swapStart = 0;
	if(first == list->head){
		swapHead = 1;
	}else if (second == list->head)
	{
		swapHead = 2;
	}
	if(first == list->start){
		swapStart = 1;
	}else if (second == list->start)
	{
		swapStart = 2;
	}
	if(second == list->tail){
		swapTail = 1;
	}else if (first == list->tail)
	{
		swapTail = 2;
	}
    TNo * auxiliar = NULL;

    auxiliar = first->next;
    first->next = second->next;
    second->next = auxiliar;
    if(first->next != NULL){
	  first->next->prev = first;
    }
    if(second->next != NULL){
	  second->next->prev = second;
    }
    auxiliar = first->prev;
    first->prev = second->prev;
    second->prev = auxiliar;
    if(first->prev != NULL){
	  first->prev->next = first;
    }
    if(second->prev != NULL){
	  second->prev->next = second;
    }

    switch(swapHead){
    	case 1:
    		list->head = second;
    	break;
    	case 2:
    		list->head = first;
    	break;
    }

    switch(swapStart){
    	case 1:
    		list->start = second;
    	break;
    	case 2:
    		list->start = first;
    	break;
    }

    switch(swapTail){
    	case 1:
    		list->tail = first;
    	break;
    	case 2:
    		list->tail = second;
    	break;
    }

    list->first = second;
    list->second = first;
    return list;
}



TList * quickSort(TList * list, TNo * left, TNo * right) {
	list->head = left;
	list->tail = right;
	TNo * i = list->head;
	TNo * j = list->tail;

	int pivot = list->head->value;
	/* partition */
	while (beforeEqual(i,j)) {
		while (i->value < pivot)
			i = i->next;
		while (j->value > pivot)
			j = j->prev;
		if (beforeEqual(i,j)) {
			list->first = i;
			list->second = j;
			list = swap(list,list->first,list->second);
			i = list->first;
			j = list->second;
			i = i->next;
			j = j->prev;
		}
	};
	left = list->head;
	right = list->tail;
	/* recursion */
	if (before(left,j))
		list = quickSort(list, left, j);
	if (before(i,right))
		list = quickSort(list, i, right);

	return list;
}

int main(){
	TNo * head = NULL;
    
    timeval time;
    gettimeofday(&time, NULL);
    srand(time.tv_usec);
    int op = 0;
    TNo * aux = NULL;
    head = createTNo(rand()%100,NULL);
    aux = createTNo(rand()%100,head);
    for (int i = 100; i >= 2; --i)
    {
       aux = createTNo(rand()%100,aux);
    }
    TList * list = createTList(head, aux);
	do{
        printf("Digite a opção: \n");
        printf("(0-Sair, \n1-Listar, \n");
        printf("2-Ordenar)\n");
        scanf("%d",&op);
        switch(op){
            case 1:
            	listAllContacts(list->start);
            break;
            case 2:
            	list = quickSort(list, list->head, list->tail);
            	listAllContacts(list->start);
            break;
        }
    }while(op != 0);    
    return 0;
}