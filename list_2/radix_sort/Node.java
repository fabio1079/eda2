class Node<Template> {
  private Template data;
  private Node<Template> next = null;

  public Node(){}
  public Node(Template data) {
    this.data = data;
  }

  public void setData(Template data) {
    this.data = data;
  }

  public Template getData() {
    return this.data;
  }

  public void setNext(Node<Template> next) {
    this.next = next;
  }

  public Node<Template> getNext() {
    return this.next;
  }
}