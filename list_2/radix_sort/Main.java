import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

class Main {
  public static int[] generateRandomList(int min, int max) {
    ArrayList<Integer> arrayList = new ArrayList<Integer>();
    int[] arrayResult = new int[(max-min)+1];

    for(int i = min; i <= max; i++) {
      arrayList.add(i);
    }

    Collections.shuffle(arrayList);

    for(int i = 0; i <= (max-min); i++) {
      arrayResult[i] = arrayList.get(i);
    }

    return arrayResult;
  }

  public static long getTimeIntervalCountingSort(int[] array) {
    Date start = new Date();

    CountingSort.sort(array);

    Date end = new Date();

    return end.getTime()-start.getTime();
  }

  public static long getTimeIntervalRadixSort(int[] array, int digits) {
    RadixSort radixSort = new RadixSort();
    Date start = new Date();

    radixSort.sort(array, digits);

    Date end = new Date();

    return end.getTime()-start.getTime();
  }

  public static void showDifferencesBetweenSorts(int digits) {
    int maxDigits = (int) Math.pow(10.0, (double)digits);

    int[] arrayOne = generateRandomList(0, maxDigits-1);
    int[] arrayTwo = new int[maxDigits];

    for(int i = 0; i < maxDigits; i++) {
      arrayTwo[i] = arrayOne[i];
    }

    long counting = getTimeIntervalCountingSort(arrayOne);
    long radix = getTimeIntervalRadixSort(arrayTwo, digits);

    System.out.println("CountingSort: "+counting+" milliseconds");
    System.out.println("RadixSort   : "+radix+" milliseconds");
  }

  public static void main(String [] args) {

    for(int i = 2; i < 8; i++) {
      System.out.println("\n##### Time for array with "+i+" digits values #####");
      showDifferencesBetweenSorts(i);
    }
  }
}