import java.util.LinkedList;

/*
Logic used on this sort
https://www.youtube.com/watch?v=xhr26ia4k38
*/
class RadixSort {
  private final int BUCKETS_NUMBER = 10;
  private LinkedList<Queue<Integer>> buckets = new LinkedList<Queue<Integer>>();

  public RadixSort () {
    for(int i = 0; i < BUCKETS_NUMBER; i++) {
      Queue<Integer> queue = new Queue<Integer>();

      this.buckets.add(queue);
    }
  }

  public void sort(int[] array, int digits) {
    int module_divisor = 10;
    int divisor_factor = 1;
    int bucket_position;
    int i, j, k;

    for(i = 0; i < digits; i++) {
      for(j = 0; j < array.length; j++) {
        bucket_position = array[j]%module_divisor;
        bucket_position = bucket_position/divisor_factor;

        this.buckets.get(bucket_position).add(array[j]);
      }

      k = 0;
      for(j=0; j < BUCKETS_NUMBER; j++) {
        Queue<Integer> bucketQueue = this.buckets.get(j);

        while( !bucketQueue.isEmpty() ) {
          array[k++] = bucketQueue.next();
        }
      }

      module_divisor *= 10;
      divisor_factor *= 10;
    }
  }
}
