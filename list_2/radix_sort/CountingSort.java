/*
1,6,9,6,5,1,4,8,9,3,0,1

 0 1 2 3 4 5 6 7 8 9
[0,0,0,0,0,0,0,0,0,0]

 0 1 2 3 4 5 6 7 8 9
[1,3,0,1,1,1,2,0,1,2]

0,1,1,1,3,4,5,6,6,8,9,9
*/

class CountingSort {
  public static void sort(int[] list) {
    int i;
    int maximumValue = 0;
    int[] sortedList;
    int[] countList;

    for(i = 0; i < list.length; i++) {
      if( maximumValue < list[i] ) {
        maximumValue = list[i];
      }
    }

    countList = new int[maximumValue+1];

    for( i=0; i < list.length; i++ ) {
      countList[list[i]] += 1;
    }

    int j = 0;
    for( i=0; i < countList.length; i++ ) {
      while(countList[i] > 0) {
        list[j++] = i;
        countList[i]--;
      }
    }
  }
}
