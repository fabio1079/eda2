class Queue<Template> {
  private int length;
  private Node<Template> nodeList = null;
  private Node<Template> actual = null;

  public Queue() {
    this.length = 0;
    this.nodeList = null;
  }

  public void add(Template data) {
    if( this.nodeList == null ) {
      this.nodeList = new Node<Template>();

      this.actual = this.nodeList;
    }

    this.actual.setData(data);
    this.actual.setNext(new Node<Template>());
    this.actual = this.actual.getNext();
    this.length += 1;
  }

  public boolean isEmpty() {
    return this.length == 0;
  }

  public Template next() {
    Template data = this.nodeList.getData();

    this.nodeList = this.nodeList.getNext();
    this.length -= 1;

    return data;
  }

  public int getLength() {
    return this.length;
  }
}