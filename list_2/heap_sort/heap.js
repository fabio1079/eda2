function Heap() {
  var data = [];
  var last = -1;

  function swapValues(indexA, indexB) {
    var temp = data[indexA];

    data[indexA] = data[indexB];
    data[indexB] = temp;
  }

  function parentIndex(index) {
    if( index%2 == 0 )
      return Math.floor(index/2)-1;
    else
      return Math.floor(index/2);
  }

  function leftIndex(index) {
    return (index*2 + 1);
  }

  function rightIndex(index) {
    return (index*2 + 2);
  }

  function arrangePosition(index) {
    if( index < 0) return;

    var continue_swapping = true;
    var current_index = index;
    var parent_index;

    while(continue_swapping) {
      parent_index = parentIndex(current_index);

      if( parent_index == current_index ) {
        continue_swapping = false;
      } else {
        if( data[current_index] > data[parent_index] ) {
          swapValues(current_index, parent_index);
          current_index = parent_index;
        } else {
          continue_swapping = false;
        }
      }
    }
  }

  function heapify(index) {
    var left = leftIndex(index);
    var right = rightIndex(index);
    var largest = index;

    if( left <= last && data[left] > data[largest] ) {
      largest = left;
    }

    if( right <= last && data[right] > data[largest] ) {
      largest = right;
    }

    if( largest != index ) {
      swapValues(index, largest);

      heapify(largest);
    }
  }

  return {
    add : function(value) {
      data.push(value);
      last += 1;
      arrangePosition(last);
    },

    sort : function() {
      while(last >= 0) {
        swapValues(0, last);
        last -= 1;
        heapify(0);
      }

      var sorted = data;
      data = [];

      return sorted;
    },

    sortReverse : function() {
      var sorted = [];

      while(last >= 0) {
        swapValues(0, last);
        sorted.push(data.pop());
        last -= 1;

        heapify(0);
      }

      return sorted;
    },

    getList : function() {
      return data;
    },

    heapList : function(list) {
      for(var i = 0; i < list.length; i++) {
        this.add(list[i]);
      }
    }
  };
}

function shuffle(array) {
  var counter = array.length, temp, index;

  while (counter > 0) {
    index = Math.floor(Math.random() * counter);
    counter--;

    temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

function listUpTo(max){
  var list = [];

  for(var i = 0; i < max; i++) {
    list.push(i);
  }

  return list;
}

var unsortedTo10 = shuffle(listUpTo(10)); // 10^1
var unsortedTo100 = shuffle(listUpTo(100)); // 10^2
var unsortedTo1000 = shuffle(listUpTo(1000)); // 10^3
var unsortedTo10000 = shuffle(listUpTo(10000)); // 10^4
var unsortedTo100000 = shuffle(listUpTo(100000)); // 10^5
var unsortedTo1000000 = shuffle(listUpTo(1000000)); // 10^6
var unsortedTo10000000 = shuffle(listUpTo(10000000)); // 10^7

function sortTime(list, reverse) {
  var heap = new Heap();
  heap.heapList(list);

  var start = new Date().getTime();

  if( reverse == null )
    heap.sort();
  else
    heap.sortReverse();

  var end = new Date().getTime();

  console.log("Heap sorted on :", (end-start), " miliseconds");
}


console.log("\nSorting 2 digits unsorted list");
console.log("### Normal ###");
sortTime(unsortedTo10);
console.log("### Reverse ###");
sortTime(unsortedTo10, true);

console.log("\nSorting 3 digits unsorted list");
console.log("### Normal ###");
sortTime(unsortedTo100);
console.log("### Reverse ###");
sortTime(unsortedTo100, true);

console.log("\nSorting 4 digits unsorted list");
console.log("### Normal ###");
sortTime(unsortedTo1000);
console.log("### Reverse ###");
sortTime(unsortedTo1000, true);

console.log("\nSorting 5 digits unsorted list");
console.log("### Normal ###");
sortTime(unsortedTo10000);
console.log("### Reverse ###");
sortTime(unsortedTo10000, true);

console.log("\nSorting 6 digits unsorted list");
console.log("### Normal ###");
sortTime(unsortedTo100000);
console.log("### Reverse ###");
sortTime(unsortedTo100000, true);

console.log("\nSorting 7 digits unsorted list");
console.log("### Normal ###");
sortTime(unsortedTo1000000);
console.log("### Reverse ###");
sortTime(unsortedTo1000000, true);

console.log("\nSorting 8 digits unsorted list");
console.log("### Normal ###");
sortTime(unsortedTo10000000);
console.log("### Reverse ###");
sortTime(unsortedTo10000000, true);
