#!/usr/bin/python2.7
#-*- coding: utf-8 -*-

from random import shuffle

def selection_sort(list_values):
  list_length = len(list_values)
  i, j = 0, 0

  while i < list_length:
    j = i

    while j < list_length:
      if list_values[j] < list_values[i]:
        list_values[j], list_values[i] = list_values[i], list_values[j]

      j += 1

    i += 1

  return list_values


values = range(20)
shuffle(values)

print values, "\n", selection_sort(values)

