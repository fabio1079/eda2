
def quick_sort list
  return [] if list.empty?

  position = Random.rand list.length
  position_value = list[position]

  left = []
  right = []

  list.each do |value|
    if position_value != value and value < position_value
      left.push value
    elsif position_value != value
      right.push value
    end
  end

  left = quick_sort(left)
  right = quick_sort(right)

  left + [position_value] + right
end


list = []

50000.times do
  list.push Random.rand 10000
end

before = nil
quick_sort(list).each do |value|
  before = value if before == nil

  if before > value
    p before, value
  end
end

