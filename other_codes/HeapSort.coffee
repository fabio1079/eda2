class Heap
  constructor: ->
    @data = []
    @last = -1


  swapValues: (indexA, indexB) ->
    temp = @data[indexA]

    @data[indexA] = @data[indexB]
    @data[indexB] = temp


  parentIndex: (index) ->
    if index%2 is 0
      Math.floor(index/2)-1
    else
      Math.floor(index/2)


  leftIndex: (index) ->
    index*2 + 1


  rightIndex: (index) ->
    index*2 + 2


  arrangePosition: (index) ->
    return false if index < 0

    continue_swapping = true
    current_index = index

    while continue_swapping
      parent_index = this.parentIndex current_index

      if parent_index is current_index
        continue_swapping = false

      if @data[parent_index] isnt undefined and @data[current_index] > @data[parent_index]
        this.swapValues current_index, parent_index
        current_index = parent_index
      else
        continue_swapping = false


  heapfy: (index) ->
    left = this.leftIndex index
    right = this.rightIndex index
    largest = index

    if left <= @last and @data[left] > @data[right]
      largest = left

    if right <= @last and @data[right] > @data[largest]
      largest = right

    if largest isnt index
      this.swapValues index, largest
      this.heapfy largest


  sort: ->
    while @last >= 0
      this.swapValues 0, @last--
      this.heapfy 0

    list = @data
    @data = []
    list


  add: (value) ->
    @data.push value
    @last += 1
    this.arrangePosition @last


  get: ->
    @data


a = new Heap()

[1..10].forEach (n) ->
  a.add n

console.log a.sort()
