Array.prototype.last = function() {
  return this[this.length-1];
}

/*
  A "class" to represent a ClassRoom with a class number, start time and end time.
*/
exports.ClassRoom = function(s, e) {
  var start = s;
  var end = e;
  var class_number = null;


  return {
    setStart : function(s) {
      start = s;
    },

    getStart : function() {
      return start;
    },

    setEnd : function(e) {
      end = e;
    },

    getEnd : function() {
      return e;
    },

    setClassNumber : function(cn) {
      class_number  = cn;
    },

    getClassNumber : function() {
      return class_number;
    }
  }
}

// Generate time to ClassRoom's start or end
exports.generateTime = function() {
  var timeA = Math.floor(Math.random()*5) + 8; // from 8:00 to 13:00
  var timeB;

  if( Math.random() < 0.5 )
    timeB = 0;
  else
    timeB = 3;

  var timeC = timeA.toString()+"."+timeB.toString();

  return parseFloat(timeC);
}

/*
  Compare two ClassRooms times
  return:
    -1, timeA is less than timeB
    0, timeA is equal to timeB
    1, timeA is greater than timeB
*/
exports.compareTime = function(timeA, timeB) {
  if( timeA < timeB )
    return -1;
  else if( timeA == timeB )
    return 0;
  else
    return 1;
}

/*
  Compare two ClassRooms start(uses compareTime)
  return:
    true, the value of indexA is less than the value os indexB
    false, the opposite top OR if they are equal
*/
exports.compareByStart = function(list, indexA, indexB) {
  return ( exports.compareTime(list[indexA].getStart(), list[indexB].getStart()) == -1 );
}

/*
  Compare two ClassRooms end(uses compareTime)
  return:
    true, the value of indexA is less than the value os indexB
    false, the opposite top OR if they are equal
*/
exports.compareByEnd = function(list, indexA, indexB) {
  return ( exports.compareTime(list[indexA].getEnd(), list[indexB].getEnd()) == -1 );
}

/*
  Compare two ClassRooms interval(uses compareTime)
  return:
    true, the value of indexA is less than the value os indexB
    false, the opposite top OR if they are equal
*/
exports.compareByInterval = function(list, indexA, indexB) {
  var intervalA = list[indexA].getEnd() - list[indexA].getStart();
  var intervalB = list[indexB].getEnd() - list[indexB].getStart();

  // try to minimize javascript round errors
  intervalA = parseInt(intervalA*100)/100;
  intervalB = parseInt(intervalB*100)/100;

  return ( exports.compareTime(intervalA, intervalB) == -1 );
}

/*
  Sort an array using quicksort and any function to
  do the comparison
*/
exports.sortBy = function(class_list, compare_function) {
  function quickSort(list, start, end) {
    var left_index  = start,
        right_index = end,
        pivot       = parseInt((left_index+right_index)/2),
        aux         = null;

    while( left_index < right_index ) {
      while( compare_function(list, left_index, pivot) ) left_index++;
      while( compare_function(list, pivot, right_index) ) right_index--;

      if( left_index <= right_index ) {
        aux = list[left_index];
        list[left_index] = list[right_index];
        list[right_index] = aux;

        left_index++;
        right_index--;
      }
    }

    if( right_index > start ) quickSort(list, start, right_index, compare_function);

    if( left_index < end ) quickSort(list, left_index, end, compare_function);
  }

  if( class_list.length > 1 ) quickSort(class_list, 0, class_list.length-1);
}


exports.reverseArrayWithOutReference = function(arr) {
  var list = [];

  for(var i = arr.length-1; i >= 0; i--) list.push(arr[i]);

  return list;
}

// Generate a lista os classes
exports.generateClasses = function(max_classes) {
  var classes = [];

  while( classes.length < max_classes ) {
    var classroom,
        timeA = exports.generateTime(),
        timeB = exports.generateTime();

    if( timeA != timeB ) { // We dont need a class that start and end at the same time
      if( exports.compareTime(timeA, timeB) == -1 )
        classroom = new exports.ClassRoom(timeA, timeB);
      else
        classroom = new exports.ClassRoom(timeB, timeA);

      classes.push(classroom);
    }
  }

  return classes;
}

// Remove classes with a class number
exports.removeClasses = function(class_list, class_number) {
  var list = [];

  for(var i = 0, len = class_list.length; i < len; i++) {
    if( class_list[i].getClassNumber() != class_number ) {
      list.push(class_list[i]);
    }
  }

  class_list = [];
  for(var i = 0, len = list.length; i < len; i++) {
    class_list.push(list[i]);
  }

  return class_list;
}

exports.scheduleClasses = function(sorted_classes){
  var classes = sorted_classes;
  
  var class_number = 1;
  var classes_added = [[]];

  while(classes.length > 0) {
    if( classes_added[class_number-1].length == 0 ) {
      classes[0].setClassNumber(class_number);
      classes_added[class_number-1].push(classes[0]);
    }

    for( var i = 1; i < classes.length; i++ ) {
      if( classes_added[class_number-1].last().getEnd() <= classes[i].getStart() ) {
        classes[i].setClassNumber(class_number);
        classes_added[class_number-1].push(classes[i]);
      }
    }

    classes = exports.removeClasses(classes, class_number);
    class_number++;
    classes_added.push([]);
  }

  classes_added.pop();

  return classes_added;
}

exports.scheduleClassesByStartTime = function(classes) {
  exports.sortBy(classes, exports.compareByStart);

  classes_added = exports.scheduleClasses(classes);

  return classes_added;
}

exports.scheduleClassesByEndTime = function(classes) {
  exports.sortBy(classes, exports.compareByEnd);

  classes_added = exports.scheduleClasses(classes);

  return classes_added;
}

exports.scheduleClassesByIntervalTime = function(classes) {
  exports.sortBy(classes, exports.compareByInterval);

  classes_added = exports.scheduleClasses(classes);

  return classes_added;
}