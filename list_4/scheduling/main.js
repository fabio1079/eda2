var modules = require("./modules");

const TOTAL_CLASSES = 8;

var to_do_list = [0,1,2,3,4,5],
    tests, classes_normal, classes_reversed;

do {
  tests = [];
  classes_normal = modules.generateClasses(TOTAL_CLASSES);
  classes_reversed = modules.reverseArrayWithOutReference(classes_normal);

  if( to_do_list.indexOf(0) != -1 ) {
    tests.push({
      method : "sorted by start",
      index  : 0,
      allocated_rooms   : modules.scheduleClassesByStartTime(classes_normal)
    });
  }

  if( to_do_list.indexOf(1) != -1 ) {
    tests.push({
      method : "sorted by end",
      index  : 1,
      allocated_rooms   : modules.scheduleClassesByEndTime(classes_normal)
    });
  }

  if( to_do_list.indexOf(2) != -1 ) {
    tests.push({
      method : "sorted interval",
      index  : 2,
      allocated_rooms   : modules.scheduleClassesByIntervalTime(classes_normal)
    });
  }

  if( to_do_list.indexOf(3) != -1 ) {
    tests.push({
      method : "sorted by start reversed",
      index  : 3,
      allocated_rooms   : modules.scheduleClassesByStartTime(classes_reversed)
    });
  }

  if( to_do_list.indexOf(4) != -1 ) {
    tests.push({
      method : "sorted by end reversed",
      index  : 4,
      allocated_rooms   : modules.scheduleClassesByEndTime(classes_reversed)
    });
  }

  if( to_do_list.indexOf(5) != -1 ) {
    tests.push({
      method : "sorted interval reversed",
      index  : 5,
      allocated_rooms   : modules.scheduleClassesByIntervalTime(classes_reversed)
    });
  }

  var bigger = {index: -1, length: 0, method:""};

  for(var i = 0; i < tests.length; i++) {
    if( tests[i].allocated_rooms.length > bigger.length ) {
      bigger.length = tests[i].allocated_rooms.length;
      bigger.index = tests[i].index;
      bigger.method = tests[i].method;
    }
  }

  if(tests.length > 1) {
    to_do_list.splice(to_do_list.indexOf(bigger.index), 1);
    console.log("Removed: "+bigger.method);
  }

} while( tests.length > 1 );

console.log("\n\t--- Best: "+tests[0].method+" ---\n");
