﻿import random

def interpolation_search(value, list_values):
    inferior = 0
    superior = len(list_values)-1
    while (inferior <= superior):
		possible_position = inferior + (superior - inferior) * ((value-list_values[inferior])//(list_values[superior]-list_values[inferior]))
		if(value == list_values[possible_position]):
			return possible_position
		elif(value > list_values[possible_position]):
			inferior = possible_position + 1
		else:
			superior = possible_position - 1
    return -1
    
values = random.sample(xrange(1000000), 1000000)
values.sort()
print(interpolation_search(values[3],values))