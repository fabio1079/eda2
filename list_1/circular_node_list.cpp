#include <cstdlib>
#include <iostream>

struct Node {
  int value;
  Node *next;
  Node *prev;
};

class CircularNodeList {
private:
  int length;
  Node *head;
  Node *tail;

  Node *buildNode(int value);
  void swapNodes(Node *left, Node *right);
  void swapNeighborsNodes(Node *left, Node *right);
  void swapDistantNodes(Node *left, Node *right);
  bool isNeighborsNodes(Node *left, Node *right);
  int findBetween(Node *node, int value, int actual_position, int minimum_position, int maximum_position);

public:
  CircularNodeList();
  ~CircularNodeList();
  int size();
  void append(int value);
  void prepend(int value);
  void insert(int position, int value);
  Node *get(int position);
  Node *pop();
  Node *shift();
  Node *remove(int position);
  void sort();
  int find(int value);
};

CircularNodeList::CircularNodeList() {
  this->length = 0;
  this->head = NULL;
  this->tail = NULL;
}

CircularNodeList::~CircularNodeList() {
  this->head->prev = NULL;
  this->tail->next = NULL;
  Node *temp;

  while(this->head != NULL) {
    temp = this->head;
    this->head = this->head->next;

    delete(temp);
  }
}

Node *CircularNodeList::buildNode(int value) {
  Node *node = new Node;

  node->value = value;
  node->next = NULL;
  node->prev = NULL;

  return node;
}

bool CircularNodeList::isNeighborsNodes(Node *left, Node *right) {
  bool neighbors_nodes = false;

  if( left->next != NULL && left->next->value == right->value ) {
    neighbors_nodes = true;
  }

  if( left->prev != NULL && left->prev->value == right->value ) {
    neighbors_nodes = true;
  }

  if( right->next != NULL && right->next->value == left->value ) {
    neighbors_nodes = true;
  }

  if( right->prev != NULL && right->prev->value == left->value ) {
    neighbors_nodes = true;
  }

  return neighbors_nodes;
}

void CircularNodeList::swapDistantNodes(Node *left, Node *right) {
  Node *temp;

  if(left->prev) {
    left->prev->next = right;
  }

  if(right->prev) {
    right->prev->next = left;
  }

  if(left->next) {
    left->next->prev = right;
  }

  if(right->next) {
    right->next->prev = left;
  }

  temp = left->prev;

  left->prev = right->prev;
  right->prev = temp;
  temp = left->next;
  left->next = right->next;
  right->next = temp;
}

void CircularNodeList::swapNeighborsNodes(Node *left, Node *right) {
  Node *left_next, *right_prev;

  right_prev = right->prev;
  left_next = left->next;

  if( right_prev != NULL) {
    right_prev->next = left;
  }

  if(left_next != NULL) {
    left_next->prev = right;
  }

  left->next = right;
  left->prev = right_prev;

  right->prev = left;
  right->next = left_next;
}

void CircularNodeList::swapNodes(Node* left, Node* right) {
  Node *newHead=NULL, *newTail=NULL;

  if(left->prev == NULL) {
    newHead = right;
  }

  if( right->prev == NULL ) {
    newHead = left;
  }

  if( left->next == NULL ) {
    newTail = right;
  }

  if( right->next  == NULL ) {
    newTail = left;
  }

  if( this->isNeighborsNodes(left, right) ) {
    this->swapNeighborsNodes(left, right);
  } else {
    this->swapDistantNodes(left, right);
  }

  if( newHead != NULL ) {
    this->head = newHead;
  }

  if( newTail != NULL ) {
    this->tail = newTail;
  }
}

int CircularNodeList::findBetween(Node *node, int value, int actual_position, int minimum_position, int maximum_position) {
  int middle = (minimum_position+maximum_position)/2;

  if( actual_position < middle ) {
    while( actual_position < middle ) {
      node = node->next;
      actual_position++;
    }
  } else {
    while( actual_position > middle ) {
      node = node->prev;
      actual_position--;
    }
  }

  if( minimum_position+1 == maximum_position ) {
    return (node->value == value ? actual_position : -1);
  }

  if( node->value == value ) {
    return actual_position;
  } else if( node->value < value  ) {
    return this->findBetween(node, value, actual_position, middle, maximum_position);
  } else {
    return this->findBetween(node, value, actual_position, minimum_position, middle);
  }
}

int CircularNodeList::size() {
  return this->length;
}

void CircularNodeList::append(int value) {
  Node *node = this->buildNode(value);

  if(this->length == 0) {
    this->head = node;
    this->tail = node;
  } else if( this->length == 1 ) {
    node->prev = this->head;
    node->next = this->head;

    this->head->next = node;
    this->head->prev = node;

    this->tail = node;
  } else {
    node->prev = this->tail;
    node->next = this->head;

    this->tail->next = node;
    this->tail = node;
  }

  this->length++;
}

void CircularNodeList::prepend(int value) {
  Node *node = this->buildNode(value);

  if(this->length == 0) {
    this->head = node;
    this->tail = node;
  } else if( this->length == 1 ) {
    node->prev = this->tail;
    node->next = this->tail;

    this->tail->next = node;
    this->tail->prev = node;

    this->head = node;
  } else {
    node->prev = this->tail;
    node->next = this->head;

    this->head->prev = node;
    this->head = node;
  }

  this->length++;
}

void CircularNodeList::insert(int position, int value) {
  Node *node, *node_left, *node_right;
  int index = 0;

  if( position <= 0 ) {
    this->prepend(value);
  } else if( position >= this->length-1 ) {
    this->append(value);
  } else {
    node = this->buildNode(value);
    node_right = this->head;

    while( index < position ) {
      node_right = node_right->next;
      index++;
    }

    node_left = node_right->prev;

    node->prev = node_left;
    node->next = node_right;

    node_left->next = node;
    node_right->prev = node;
  }

  this->length++;
}

Node *CircularNodeList::get(int position) {
  Node *node = NULL;
  int index, middle;

  if( position >= 0 && position < this->length ) {
    middle = this->length/2;

    if( position < middle ) {
      index = 0;
      node = this->head;

      while( index < position ) {
        node = node->next;
        index++;
      }
    } else {
      index = this->length-1;
      node = this->tail;

      while( index > position ) {
        node = node->prev;
        index--;
      }
    }
  }

  return node;
}

Node *CircularNodeList::pop() {
  Node *node = NULL;

  if( this->length > 0 ) {
    node = this->tail;

    this->tail = this->tail->prev;
    this->tail->next = this->head;
    this->head->prev = this->tail;

    node->next = NULL;
    node->prev = NULL;

    this->length--;
  }

  return node;
}

Node *CircularNodeList::shift() {
  Node *node = NULL;

  if( this->length > 0 ) {
    node = this->head;

    this->head = this->head->next;
    this->head->prev = this->tail;
    this->tail->next = this->head;

    node->next = NULL;
    node->prev = NULL;

    this->length--;
  }

  return node;
}

Node *CircularNodeList::remove(int position) {
  Node *node = NULL;

  if( position == 0 ) {
    node = this->shift();
  } else if( position == this->length-1 ) {
    node = this->pop();
  } else if( position > 0 && position < this->length-1 ) {
    Node *node_left, *node_right;
    node = this->get(position);

    node_left = node->prev;
    node_right = node->next;

    node_left->next = node_right;
    node_right->prev = node_left;

    node->next = NULL;
    node->prev = NULL;

    this->length--;
  }

  return node;
}

void CircularNodeList::sort() {
  if( this->length <= 1 ) {
    return;
  }

  this->head->prev = NULL;
  this->tail->next = NULL;

  Node *temp, *actual_position, *minimum_node;
  bool changed_minimum;

  actual_position = this->head;
  minimum_node = NULL;

  while( actual_position->next != NULL ) {
    changed_minimum = false;
    temp = actual_position->next;
    minimum_node = actual_position;

    while( temp != NULL ) {
      if( temp->value < minimum_node->value ) {
        minimum_node = temp;
        changed_minimum = true;
      }

      temp = temp->next;
    }

    if( changed_minimum ) {
      this->swapNodes(minimum_node, actual_position);
    }

    actual_position = minimum_node->next;
  }

  this->head->prev = this->tail;
  this->tail->next = this->head;
}

int CircularNodeList::find(int value) {
  Node *node = this->head;

  return this->findBetween(node, value, 0, 0, this->length);
}

int main() {
  int i;
  CircularNodeList l;

  for(i = 0; i < 30000; i++) {
    l.prepend(i);
  }

  std::cout << "List filled with numbers from 30000 to 0\n";

  l.sort();

  std::cout << "List sorted\n";

  for(i=0; i <= 30000; i += 250) {
    std::cout << "Value: " << i << "\tFound at: " << l.find(i) << "\n";
  }

  return 0;
}
