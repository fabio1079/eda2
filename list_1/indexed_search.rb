class IndexedList < Array
  MINIMUM_WINDOW_SIZE = 30
  WINDOW_SIZE_DIVIDER = 0.1

  def initialize *args
    super(args)

    @indexed_table = []
  end

  def build_indexes
    window_size = get_window_size

    if window_size > MINIMUM_WINDOW_SIZE
      @indexed_table = []
      self.sort!

      (0..self.length-1).step(window_size) do |index|
        @indexed_table.push [index, self[index]]
      end
    end

    @indexed_table
  end

  def find value
    if @indexed_table.empty?
      find_between_interval value
    else
      find_by_index_table value
    end
  end

  private

  def get_window_size
    (self.length*WINDOW_SIZE_DIVIDER).to_i
  end

  def find_by_index_table value
    indexes_interval = seek_indexes_interval value

    find_between_interval value, indexes_interval.first, indexes_interval.last
  end

  def seek_indexes_interval value
    before_visited_index = nil
    last_visited_index = nil

    @indexed_table.each do |row_value|
      if value >= row_value.last
        before_visited_index = row_value.first
      else
        last_visited_index = row_value.first
        break
      end
    end

    [before_visited_index, last_visited_index]
  end

  def find_between_interval value, first_interval=0, last_interval=nil
    last_interval = self.length if last_interval.nil?

    (first_interval..last_interval).each do |index|
      return index if value == self[index]
    end

    nil
  end
end

list = IndexedList::new

(0..10000).step(3) do |number|
  list.push number
end

list.build_indexes

p list.find 0
p list.find 77
p list.find 123
p list.find 1235
p list.find 7890


