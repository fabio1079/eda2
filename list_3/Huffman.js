function Heap() {
  var values = {
    char : "",
    count: 0
  };

  var data = [];
  var last = -1;

  function swapValues(indexA, indexB) {
    var temp = data[indexA];

    data[indexA] = data[indexB];
    data[indexB] = temp;
  }

  function parentIndex(index) {
    if( index%2 == 0 )
      return Math.floor(index/2)-1;
    else
      return Math.floor(index/2);
  }

  function leftIndex(index) {
    return (index*2 + 1);
  }

  function rightIndex(index) {
    return (index*2 + 2);
  }

  function arrangePosition(index) {
    if( index < 0) return;

    var continue_swapping = true;
    var current_index = index;
    var parent_index;

    while(continue_swapping) {
      parent_index = parentIndex(current_index);

      if( parent_index == current_index ) {
        continue_swapping = false;
      } else {
        if( data[parent_index] != undefined && data[current_index].count > data[parent_index].count ) {
          swapValues(current_index, parent_index);
          current_index = parent_index;
        } else {
          continue_swapping = false;
        }
      }
    }
  }

  function heapify(index) {
    var left = leftIndex(index);
    var right = rightIndex(index);
    var largest = index;

    if( left <= last && data[left].count > data[largest].count ) {
      largest = left;
    }

    if( right <= last && data[right].count > data[largest].count ) {
      largest = right;
    }

    if( largest != index ) {
      swapValues(index, largest);

      heapify(largest);
    }
  }

  return {
    add : function(char, count) {
      var value = {
        "char" : char,
        "count": count
      };

      data.push(value);
      last += 1;
      arrangePosition(last);
    },
    
    getList : function() {
      return data;
    }
  };
}

var word = "the lord of the rings and the fellowship of the ring";

function countLetters(word) {
  var letters = [];

  for(var i = 0; i < 255; i++) letters[i] = 0;

  for(var i = 0; i < word.length; i++) {
    letters[word.charCodeAt(i)] += 1;
  }

  return letters;
}

var a = countLetters(word);
var heap = new Heap();

for(var i = 0; i < a.length; i++) {
  if( a[i] != 0 ) {
    heap.add(String.fromCharCode(i), a[i]);
  }
}

/*
[ { char: ' ', count: 10 },
  { char: 'e', count: 5 },
  { char: 'h', count: 5 },
  { char: 'i', count: 3 },
  { char: 'o', count: 4 },
  { char: 'r', count: 3 },
  { char: 't', count: 4 },
  { char: 'a', count: 1 },
  { char: 'l', count: 3 },
  { char: 'n', count: 3 },
  { char: 'f', count: 3 },
  { char: 'p', count: 1 },
  { char: 'g', count: 2 },
  { char: 's', count: 2 },
  { char: 'd', count: 2 },
  { char: 'w', count: 1 } ]
*/
var heap_list = heap.getList();
var codes = [];
var code = "";

for(var i = 0; i < heap_list.length; i++) {
  code += String(i%2);

  codes[i] = {
    char : heap_list[i].char,
    "code" : code
  };
}

console.log(codes);
/*
[ { char: ' ', code: '0' },
  { char: 'e', code: '01' },
  { char: 'h', code: '010' },
  { char: 'i', code: '0101' },
  { char: 'o', code: '01010' },
  { char: 'r', code: '010101' },
  { char: 't', code: '0101010' },
  { char: 'a', code: '01010101' },
  { char: 'l', code: '010101010' },
  { char: 'n', code: '0101010101' },
  { char: 'f', code: '01010101010' },
  { char: 'p', code: '010101010101' },
  { char: 'g', code: '0101010101010' },
  { char: 's', code: '01010101010101' },
  { char: 'd', code: '010101010101010' },
  { char: 'w', code: '0101010101010101' } ]
*/