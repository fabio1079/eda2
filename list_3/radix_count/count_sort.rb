
# Override operators + and > of nil object
class NilClass
  def + value
    value
  end

  def > value
    false
  end
end

class CountSort
  def self.sort list
    counting_list = []
    sorted_list = []

    list.each do |value|
      counting_list[value] = counting_list[value] + 1
    end

    counting_list.each_with_index do |value, index|
      while counting_list[index] > 0
        sorted_list << index
        counting_list[index] -= 1
      end
    end

    sorted_list
  end
end
