class RadixSort
  def self.sort list, digit
    return list if digit < 0

    buckets =  []
    sorted = []

    list.each do |value|
      msd = ndigit(value, digit)

      if buckets[msd].nil?
        buckets[msd] = [value]
      else
        buckets[msd] << value
      end
    end

    buckets.each do |local_list|
      sorted.concat(sort(local_list, digit-1)) unless local_list.nil?
    end

    sorted
  end

  def self.ndigit value, digit
    while digit > 0
      value /= 10
      digit -= 1
    end

    value%10
  end
end

rs = RadixSort::sort [159, 321,852, 147, 654, 999, 987, 123, 100, 159, 852, 258, 456, 147], 2

p "="*80, rs, "="*80
