/**
 * Continuous Lines. 
 * 
 * Click and drag the mouse to draw a line. 
 */
import java.util.Arrays;

int drawDelay = 1000/30;
int[] vector;
int[] drawVector;
int size;
int[] auxiliar;
int widthStep;
int heightStep;
void setup() {
  size(640, 360);
  size = int(random(50,100));
  vector = new int[size];
  for(int i =0; i < size; ++i){
    vector[i] = int(random(size));
  }
  auxiliar = new int[size];
  drawVector = new int[size];
  heightStep = floor(height/max(vector));
  widthStep = floor(width/size);
  thread("mergeSort");
}

void draw() {
  if(!Arrays.equals(drawVector,vector)){
    stroke(0);
    arrayCopy(vector,drawVector);
    background(102);
    for(int i = 0 ; i < drawVector.length; i++) {
      rect(i*widthStep, height, widthStep, -drawVector[i]*heightStep);
    }
  }
}

void mergeSort(){
  try{
    mergeSort(0,size-1);
  }catch(InterruptedException ie){
    ie.printStackTrace();
  }
}

void mergeSort(int left, int right) throws InterruptedException{
  if (left < right) {
    int middle = left + (right - left) / 2;
    // Sort left
    Thread.sleep(drawDelay);
    
    mergeSort(left, middle);
    
    // Sort right
    Thread.sleep(drawDelay);
    
    mergeSort(middle + 1, right);
    
    // Merge
    Thread.sleep(drawDelay);
    merge(left, middle, right);
  }
}
  
void merge(int left, int middle, int right) {
  
  for (int i = left; i <= right; i++) {
    auxiliar[i] = vector[i];
  }

  int i = left;
  int j = middle + 1;
  int k = left;
  
  while (i <= middle && j <= right) {
    if (auxiliar[i] <= auxiliar[j]) {
      vector[k] = auxiliar[i];
      i++;
    } else {
      vector[k] = auxiliar[j];
      j++;
    }
    k++;
  }
  
  while (i <= middle) {
    vector[k] = auxiliar[i];
    k++;
    i++;
  }
}


void wait(int delay){
  int currentTime = millis();
  while(millis()<currentTime+delay){
    continue;
  }
}
